export interface Product {
  id: string;
  code?: number;
  number: string;
  block?: number;
  purse?: number;
  name: string;
  type: string;
  type_original?: string;
  save_type_original?: string;
  status: string;
  subproductCode?: number;
  props: any;
  first?: any;
  availableBalance?: number;
  relation?: string;
  isInsufficientBalance?: boolean;
  isBlocked?: boolean;
  hasCardInformation?: boolean;
  hasDetails?: boolean;
  frequentTraveler?: number;
  lineLimit?: number;
  disbursementValue?: number;
  alliedCode?: number;
  productBlockedStatus?: {
    type: {
      notZero: boolean;
      notFourNotFive: boolean;
      fourOrFive: boolean;
    };
    moment: {
      oneOrTwo: boolean;
      threeOrFour: boolean;
    };
  };
  showProgressBar?: boolean;
  allowDebits?: boolean;
  allowDigital?: boolean;
  numberToDisplay?: string;
  currentLabelError?: string;
  loans_details?: {
    vehicle?: {
      name?: string;
      plate?: string;
      model_year?: number;
    };
    insurances?: [
      {
        code?: string;
        number?: string;
        description?: string;
      }
    ];
    available_quota?: number;
    credit_value: number;
    total_credit_balance: number;
    capital_balance: number;
    disbursement_date: string;
    entity_name?: string;
    guarantee_code?: string;
    term?: number;
    city?: string;
    property_code?: string;
  };
  payment_details?: {
    default_interest?: number;
    total_default?: number;
    last_payment_value: number;
    last_payment_date: string;
    annual_effective_rate: number;
    monthly_rate_due: number;
    deadline_date: string;
    loan_cut_date: string;
    days_past_due: number;
    number_quota: number;
    payment_value?: number;
    account_number?: string;
    account_name?: string;
    account_signature?: string;
    account_type?: string;
    balance?: number;
    block?: number;
    cardNumber?: number;
    contractNumberPCI?: number;
    pciSignature?: string;
    domestic_payment?: string;
    next_payment?: {
      total_value: number;
      payment_capital: number;
      payment_interest: number;
      payment_default_interest: number;
      payment_other_charges: number;
    };
  };
  volunteerInsuranceValues?: {
    policyStatus: string;
    paymentFrequency: string;
    lastPayment: string;
    nextPaymentDate: string;
    premiumValue: number;
    openingDate: string;
    renewalDatePolicy: string;
    insurerName: string;
  };
}
