import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { CreditsLabels } from 'src/app/shared/constants/credits-labet';
import { CreditsService } from 'src/app/shared/service/credits.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public products: Product[];
  public selectProducts: Product[];
  public flag: boolean;
  public timeline: [1, 3, 9];
  public LABELS = CreditsLabels.APP_LABELS.CREDITS.DASBOARD;


  constructor(private service: CreditsService, private route: ActivatedRoute) { }

  public ngOnInit(): void {
    this.service.getCredits().subscribe((data) => ((this.products = data.slice(0, 4)), (this.selectProducts = this.products)
    ));
  }

  public filterCredits(name: string): void {
    if (!name) {
      this.selectProducts = this.products;
    } else {
      this.selectProducts = this.products.filter((product) => {
        if (name.toLocaleLowerCase() === product.name.toLocaleLowerCase()) {
          return product.name.toLocaleLowerCase() === name.toLocaleLowerCase();
        }
      });
    }
  }

  public showInfoCredit(productId: string): void {
      this.flag = true;
      this.selectProducts = this.products.filter((product, index) => {
        return product.id === productId;
      });
    }
}
