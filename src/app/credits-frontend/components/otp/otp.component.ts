import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UrlConstant } from 'src/app/shared/constants/url-constants';
import { Router } from '@angular/router';
import { CreditsLabels } from 'src/app/shared/constants/credits-labet';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss'],
})
export class OtpComponent implements OnInit {
  public otpForm: FormGroup;
  public hasError: boolean;
  public LABELS = CreditsLabels.APP_LABELS.CREDITS.OTP;


  constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.formInit();
  }

  public sentOtp(): void {
    console.log(this.otpForm.controls.tokens.value);
    const toknes = [
      this.otpForm.controls.tokens.value.token1,
      this.otpForm.controls.tokens.value.token2,
      this.otpForm.controls.tokens.value.token3,
      this.otpForm.controls.tokens.value.token4,
    ];
    if (toknes.join('') === '5555') {
      this.router.navigate([
        UrlConstant.PATHS.CREDITS.ROOT +
          '/' +
          UrlConstant.PATHS.CREDITS.DASHBOARD,
      ]);
    } else {
      this.hasError = true;
    }

    // this.otpForm.controls.tokens.controls.token1;
  }

  getFullText(): string {
    const token1 = this.otpForm.controls.tokens.value.token1;

    const token2 = this.otpForm.controls.token2.value.token2;
    const token3 = this.otpForm.controls.token3.value.token3;
    return `${token1}${token2}${token3}`;
  }

  private formInit(): void {
    this.otpForm = this.formBuilder.group({
      tokens: this.formBuilder.group({
        token1: new FormControl([null]),
        token2: new FormControl([null]),
        token3: new FormControl([null]),
        token4: new FormControl([null]),
      }),
    });
  }
}
