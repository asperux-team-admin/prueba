import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreditsLabels } from 'src/app/shared/constants/credits-labet';
import { UrlConstant } from 'src/app/shared/constants/url-constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public hasError: boolean;
  public loginForm: FormGroup;
  public LABELS = CreditsLabels.APP_LABELS.CREDITS.LOGIN;

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {
    this.formInit();
  }

  public goValidateForm(): void {
    this.hasError = false;
    if (this.loginForm.controls.email.value === 'admin@gmail.com') {
      this.router.navigate([
        UrlConstant.PATHS.CREDITS.ROOT + '/' + UrlConstant.PATHS.CREDITS.OTP,
      ]);
    } else {
      this.hasError = true;
    }
  }

  private formInit(): void {
    this.loginForm = this.formBuilder.group({
      email: [
        null,
        [
          Validators.required,
          Validators.email,
          Validators.minLength(3),
          Validators.maxLength(320),
        ],
      ],
    });
  }
}
