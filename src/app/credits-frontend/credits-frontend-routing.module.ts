import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OtpComponent } from './components/otp/otp.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UrlConstant } from '../shared/constants/url-constants';

const routes: Routes = [
  {
    path: UrlConstant.PATHS.CREDITS.ROOT,
    component: LoginComponent,
    pathMatch: 'full'
  },
  { path: UrlConstant.PATHS.CREDITS.OTP, component: OtpComponent },
  { path: UrlConstant.PATHS.CREDITS.DASHBOARD, component: DashboardComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreditsFrontendRoutingModule {}
