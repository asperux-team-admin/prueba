import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { OtpComponent } from './components/otp/otp.component';
import { CreditsFrontendRoutingModule } from './credits-frontend-routing.module';

@NgModule({
  imports: [CommonModule, CreditsFrontendRoutingModule, SharedModule, FormsModule, ReactiveFormsModule],
  declarations: [LoginComponent, OtpComponent, DashboardComponent]
})
export class CreditsFrontendModule {}
