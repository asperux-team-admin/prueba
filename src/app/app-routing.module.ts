import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { UrlConstant } from './shared/constants/url-constants';

const routes: Routes = [
  {
    path: UrlConstant.PATHS.CREDITS.ROOT,
    loadChildren: () => import('./credits-frontend/credits-frontend.module').then((m) => m.CreditsFrontendModule)
  },
  { path: '**', redirectTo: UrlConstant.PATHS.CREDITS.ROOT }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
