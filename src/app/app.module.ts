import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreditsLabels } from './shared/constants/credits-labet';
import { CreditsService } from './shared/service/credits.service';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, SharedModule, FormsModule, HttpClientModule],
  providers: [CreditsLabels, CreditsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
