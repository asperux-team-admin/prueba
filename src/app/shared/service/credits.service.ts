import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class CreditsService {
  static URL_CREDITS = '../../../assets/debit-card.json';

  constructor(private http: HttpClient) {}

  public getCredits(): Observable<Product[]> {
    return this.http.get(CreditsService.URL_CREDITS)
    .pipe(map((response: Product[]) => response));
  }
}
