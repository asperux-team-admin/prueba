import { Injectable } from '@angular/core';
@Injectable()
export class CreditsLabels {
  static readonly APP_LABELS = {
    CREDITS: {
      LOGIN: {
        NOTIFICATION: 'Por favor vuelve a ingresar los datos email es incorrecto',
        CONTINUE: 'Continuar'
      },
      OTP: {
        NOTIFICATION: 'Por favor vuelve a ingresar los datos OTP es incorrecto',
      },
      DASBOARD: {
        TIMELINE: 'Movimientos'
      },
    }
  };
}
