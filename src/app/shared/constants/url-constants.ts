export class UrlConstant {
  static readonly PATHS = {
    CREDITS: {
      ROOT: '',
      OTP: 'otp',
      DASHBOARD: 'dashboard'
    }
  };
}
